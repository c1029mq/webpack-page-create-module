const path = require("path");
var webpack = require('webpack');
var glob = require('glob')
var HtmlWebpackPlugin = require("html-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var nodeExternals = require('webpack-node-externals');
var CopyWebpackPlugin = require("copy-webpack-plugin");
console.log(process.env.NODE_ENV)
function getEntry(globPath, pathDir) {
  var files = glob.sync(globPath);
  var entries = {},
      entry, dirname, basename, pathname, extname;

  for (var i = 0; i < files.length; i++) {
      entry = files[i];
      dirname = path.dirname(entry);
      extname = path.extname(entry);
      basename = path.basename(entry, extname);
      console.log('​getEntry -> basename', basename);
      pathname = path.join(dirname, basename);
      pathname = pathDir ? pathname.replace(pathDir, '') : pathname;
      entries[basename] = './' + entry;
  }
  return entries;
}
var htmls = getEntry('./src/view/**/*.html', 'src\\view\\');
var HtmlPlugin = [];
var entries = {};
for (var key in htmls) {
    entries[key] = htmls[key].replace('.html', '.js')
    HtmlPlugin.push(new HtmlWebpackPlugin({
      filename: (key == 'index' ? `${key}/index.html` : key + '/' + key + '.html'), 
      template: path.join(__dirname, htmls[key]),
      inject: true,
      chunks: [key, 'common']
    }))
}
// config.entry = Object.assign({}, config.entry, entries )
// config.plugins = config.plugins.concat(HtmlPlugin)
var config = {
  /*入口  --运营后台单点登录*/
  entry: Object.assign({}, {}, entries ),
  devtool: "inline-source-map",
  output: {
    path: path.join(__dirname, "./dist"),
    filename: process.env.NODE_ENV === 'dev' ? "[name]/[name].[hash].js" : "[name]/[name].[chunkhash].js",
    publicPath: "/",
    chunkFilename: "[name].[chunkhash].js"
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader' // html img引入处理
      },
      {
        test: /\.js$/,
        use: ["babel-loader?cacheDirectory=true"],
        include: path.join(__dirname, "src")
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: "css-loader"
            },
            {
              loader: "sass-loader"
            }
          ],
          // 在开发环境使用 style-loader
          fallback: "style-loader"
        }) //这里用了样式分离出来的插件，如果不想分离出来，可以直接这样写 loader:'style!css!sass'
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "url-loader",
            options: {
              // name: 'image/[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  externals: [nodeExternals()],
  plugins: [
    new UglifyJSPlugin(),
    new CleanWebpackPlugin(["dist/*"]),
    new webpack.optimize.CommonsChunkPlugin({
      name: "common",
      filename: "util/common.js",
      minChucks: 2
    }), 
    new CopyWebpackPlugin([
      {
        from: __dirname + "/src/public"
      }
    ]),
    new ExtractTextPlugin({
      filename: "[name]/[name].[contenthash:5].css",
      allChunks: true
    }),
  ].concat(HtmlPlugin),
  // 建立服务 webpack-dev-server
  devServer: {
    port: 8089,
    contentBase: "./dist",
    historyApiFallback: true,
    host: "localhost"
  }
};


// if (process.env.NODE_ENV === 'pev') {
//   config.module.rules.push(new CleanWebpackPlugin(["dist/*"]))
// }
console.log('http://localhost:8089/index/index.html')
module.exports = config;